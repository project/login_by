# Login by

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers


## Introduction

This simple Drupal 8 module allows users to log in with either their username OR email and both. 

There are additional login configration like as 
- Enable placeholder
- Enable autocomplete off
- Enable view password
- Enable login page. 

There are update login page title and login label button.

For a full description of the module, visit the
[Project page](https://www.drupal.org/project/login_by).

Submit bug reports and feature suggestions, or track changes in the
[Issue queue](https://www.drupal.org/project/issues/login_by).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Goto: /admin/config/user-interface/login_by


## Maintainers

Supporting by:

- Deepak Bhati (heni_deepak) - https://www.drupal.org/u/heni_deepak
- Radheshyam Kumawat (radheymkumar) - https://www.drupal.org/u/radheymkumar
